Uses go-libp2p library to create nodes.

Inspired from the provided examples.

# Exercises

## The exercise is the following: 

We want a network of 3 nodes N1, N2 and N3.

N1 has the data d1 that needs to go through function F1

It needs to randomly select a node netween N2 and N3 and have it execute the function F1 on d1 and send the result back to N1.


*in the following examples, d1 is the "Hello World" string and F1 is the reverseString function*

## 1. simpleTaskNode

Inspired by https://github.com/libp2p/go-libp2p-examples/tree/master/echo

*The echo protocol is replaced with a "reverse string" protocol: any string sent to a node sends the reversed string back.*

### Usage

First, 'cd simpleTaskNode'

Then, run './simpleTaskNode -l 10000' on a first terminal to set up the listening node (node B in the exercise).

Then, use the generated message in the console to start a second node (nodeA) in a second terminal.

You should see a 'Hello World' message leave Node A to node B and receive the answer, the reversed string.

## 2. peerDiscoveryNode

Inspired by https://github.com/libp2p/go-libp2p-examples/tree/master/echo and https://github.com/libp2p/go-libp2p-examples/blob/master/chat-with-rendezvous/chat.go

*I used the previous example with the "reverse string" protocol but incorporare the DHT peer discovery process from the chat-with-rendezvous example. It involves using Bootstrapping nodes and a rendez-vous string to find the other peers.*

### Usage

First, 'cd peerDiscoveryNode'

Run './peerDiscoveryNode -listen /ip4/127.0.0.1/tcp/6667' and './peerDiscoveryNode -listen /ip4/127.0.0.1/tcp/6668' on two different terminals

You should see a 'Hello World' message leave Node A to node B and receive the answer, the reversed string.

## 3. randomPeerNode

Same as the previous case but this time, a new connection is added to a connectionList and will wait for a threshold number of nodes to be connected (2 by default) before randomly chosing one of them and sending it a Hello World message.

'-passive' marks that the node will only anwer messages

'-minNodes' specifies the minimum number of connected nodes before the node sends a 'Hello World'

### Usage

First, 'cd randomPeerNode'

Run 
'./peerDiscoveryNode -listen /ip4/127.0.0.1/tcp/6667 -passive' and 
'./peerDiscoveryNode -listen /ip4/127.0.0.1/tcp/6668 -passive' on two different terminals

Then, run './peerDiscoveryNode -listen /ip4/127.0.0.1/tcp/6669' on a third terminal to spawn an active node that will first connect to the other two nodes and then chose one randomly and send a Hello World message for it to reverse it.

