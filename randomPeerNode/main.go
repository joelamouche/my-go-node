package main

import (
	"bufio"
	"context"
	"io/ioutil"
	//"reflect"
	"strconv"
	//"fmt"

	"time"
	"math/rand"

	"sync"

	libp2p "github.com/libp2p/go-libp2p"
	net "github.com/libp2p/go-libp2p-net"
	peerstore "github.com/libp2p/go-libp2p-peerstore"
	libp2pdht "github.com/libp2p/go-libp2p-kad-dht"
	multiaddr "github.com/multiformats/go-multiaddr"
	logging "github.com/whyrusleeping/go-logging"
	"github.com/ipfs/go-log"
	"github.com/libp2p/go-libp2p-discovery"
	"github.com/libp2p/go-libp2p-protocol"
)

var logger = log.Logger("rendezvous")

	
type connection struct {
    peer peerstore.PeerInfo
    stream  net.Stream
}

func main() {
	
	log.SetAllLoggers(logging.WARNING)
	log.SetLogLevel("rendezvous", "info")

	config, err := ParseFlags()

	ctx := context.Background()

	// libp2p.New constructs a new libp2p Host. Other options can be added
	// here.
	host, err := libp2p.New(ctx,
		libp2p.ListenAddrs([]multiaddr.Multiaddr(config.ListenAddresses)...),
	)
	if err != nil {
		panic(err)
	}
	logger.Info("Host created. We are:", host.ID())
	logger.Info(host.Addrs())

	// Set a stream handler on host. /reverseString/1.0.0 is
	// a user-defined protocol name.
	// This just reverses the received string and send it back
	host.SetStreamHandler("/reverseString/1.0.0", func(s net.Stream) {
		logger.Info("Got a new reverse request!")
		if err := reverseString(s); err != nil {
			logger.Warning(err)
			s.Reset()
		} else {
			s.Close()
		}
	})

	// Start a DHT, for use in peer discovery. We can't just make a new DHT
	// client because we want each peer to maintain its own local copy of the
	// DHT, so that the bootstrapping node of the DHT can go down without
	// inhibiting future peer discovery.
	kademliaDHT, err := libp2pdht.New(ctx, host)
	if err != nil {
		panic(err)
	}

	// Bootstrap the DHT. In the default configuration, this spawns a Background
	// thread that will refresh the peer table every five minutes.
	logger.Debug("Bootstrapping the DHT")
	if err = kademliaDHT.Bootstrap(ctx); err != nil {
		panic(err)
	}

	// Let's connect to the bootstrap nodes first. They will tell us about the
	// other nodes in the network.
	var wg sync.WaitGroup
	for _, peerAddr := range config.BootstrapPeers {
		peerinfo, _ := peerstore.InfoFromP2pAddr(peerAddr)
		wg.Add(1)
		go func() {
			defer wg.Done()
			if err := host.Connect(ctx, *peerinfo); err != nil {
				logger.Warning(err)
			} else {
				logger.Info("Connection established with bootstrap node:", *peerinfo)
			}
		}()
	}
	wg.Wait()

	// We use a rendezvous point "meet me here" to announce our location.
	// This is like telling your friends to meet you at the Eiffel Tower.
	logger.Info("Announcing ourselves...")
	routingDiscovery := discovery.NewRoutingDiscovery(kademliaDHT)
	discovery.Advertise(ctx, routingDiscovery, config.RendezvousString)
	logger.Debug("Successfully announced!")

	// Now, look for others who have announced
	// This is like your friend telling you the location to meet you.
	logger.Debug("Searching for other peers...")
	peerChan, err := routingDiscovery.FindPeers(ctx, config.RendezvousString)
	if err != nil {
		panic(err)
	}

	// make a channel for connected peers
	connectedPeers:= make(chan connection)

	// process connected peers
	go func () {
		count:=0
		var connectionList []connection

		for connection := range connectedPeers {
				count++

				connectionList= append(connectionList,connection)

				countStr := strconv.Itoa(count)

				logger.Info("- - - - - NEW PEER - - - - - -")
				logger.Info("Number of peers:", countStr)
				logger.Info("PEER ID ", connection.peer.ID)
				logger.Info("PPER Addrs ", connection.peer.Addrs)
				logger.Info("- - - - - *** *** - - - - - -")

				if len(connectionList)>=config.MinNumberNode&&!config.Passive {

					// randomly sleect one of the peers in the list
					rand.Seed(time.Now().UnixNano())
					randomIndex := rand.Intn(len(connectionList))
					logger.Info("Randomly chosen peer index:", string(randomIndex))

					logger.Info("Send Hello World message to peer : ", connectionList[randomIndex].peer.ID)

					_, err = connectionList[randomIndex].stream.Write([]byte("Hello, world!\n"))
					if err != nil {
						logger.Warning(err)
					}

					out, err := ioutil.ReadAll(connectionList[randomIndex].stream)
					if err != nil {
						logger.Warning(err)
					}

					logger.Info("read reply: \n", out)
				}
		}
	} ()

	for peer := range peerChan {
			if peer.ID == host.ID() {
				continue
			}
			logger.Debug("Found peer:", peer)

			logger.Debug("Connecting to:", peer)

			stream, err := host.NewStream(ctx, peer.ID, protocol.ID("/reverseString/1.0.0"))

			if err != nil {
				logger.Warning("Connection failed:", err)
				// logger.Warning("Connection failed - addr:", peer.Addrs)
				continue
			} else {

				logger.Info("Connected to:", peer)

				// add peer to connectionList, triggering the routine above

				connectedPeers <- connection{peer:peer,stream:stream}
			}
	}
}

// reverseStringRecursive
func reverseStringRecursive(input string,output string) string {
	if input == "" {
		return output
	}
	char := input[len(input)-1]
	newOutput:=output+string(char)
	return reverseStringRecursive(string(input[0:len(input)-1]),newOutput)
}

// reverseString reads a line of data a stream and writes it back
func reverseString(s net.Stream) error {
	buf := bufio.NewReader(s)
	str, err := buf.ReadString('\n')
	if err != nil {
		return err
	}

	//log.Printf("read: %s\n", str)
	logger.Info("read: \n", str)

	reversedString := reverseStringRecursive(str,"")

	//log.Printf("reversed: %s\n", reversedString)
	logger.Info("reversed: \n", reversedString)

	_, err = s.Write([]byte(reversedString))
	return err
}